package com.gaeainfo.module.base.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Caven on 2018/1/12.
 */
@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class BaseResponseDto<T> {

    private long date;

    private T data;

    private String statusCode;

    private boolean success;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
