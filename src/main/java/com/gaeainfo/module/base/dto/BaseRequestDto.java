package com.gaeainfo.module.base.dto;

/**
 * Created by Caven on 2018/1/12.
 */
public class BaseRequestDto<T> {

    private long date;

    private T data;

    private String token;


    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
