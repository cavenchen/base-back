package com.gaeainfo.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by Caven on 2018/1/7.
 */
@Configuration
@EnableWebMvc
@EnableSwagger2
@ComponentScan(basePackages = "com.gaeainfo.module.*.controller")
public class SwaggerConfig {

    public SwaggerConfig() {
    }

    @Bean
    public Docket customDocket() {
        return (new Docket(DocumentationType.SWAGGER_2)).pathMapping("/rest").select().build().apiInfo(this.apiInfo());
    }

    private ApiInfo apiInfo() {
        return (new ApiInfoBuilder()).title("Api说明").build();
    }


}
